library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

entity alu_mips is
    port ( aluInA, aluInB: in Std_Logic_Vector(31 downto 0);
           aluControl: in Std_Logic_Vector (3 downto 0);
           aluResult: out Std_Logic_Vector(31 downto 0);
           aluZero,aluOverflow: out Std_Logic);
end alu_mips;

architecture DataFlow OF alu_mips is

component aluzero_mips port (
   aluResult: in Std_Logic_Vector(31 downto 0);
   aluZero:  out Std_Logic);
end component;

   signal slt: Std_Logic;
   signal result: Std_Logic_Vector(31 downto 0);

begin
   process (aluInA,aluInB)
   begin
      if aluInA<aluInB then slt <= '1';
      else slt <= '0';
      end if;
   end process;

   process (aluInA, aluInB, aluControl,slt)
   begin
      case aluControl is
         when "0010" => result <= (aluInA + aluInB);
         when "0110" => result <= (aluInA - aluInB);
         when "0000" => result <= (aluInA and aluInB);
         when "0001" => result <= (aluInA or aluInB);
         when "0111" => result <= slt;
     end case;
   end process;

   process (result,aluControl,aluInA,aluInB)
   begin
      case aluControl is
         when "0010" => aluOverflow <= ((not aluInA(31) and not aluInB(31) and result(31)) or (aluInA(31) and aluInB(31) and not result(31)));
         when "0110" => aluOverflow <= ((not aluInA(31) and aluInB(31) and result(31)) or (aluInA(31) and not aluInB(31) and not result(31)));
      end case;
   end process;

   Zero1: aluzero_mips port map (
      aluResult => result,
      aluZero => aluZero
   );

   aluResult <= result;
end DataFlow;

