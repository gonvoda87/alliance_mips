library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

entity aluZero_mips is
    port ( aluResult: in Std_Logic_Vector(31 downto 0);
           aluZero: out Std_Logic);
end aluZero_mips;

architecture DataFlow OF aluZero_mips is
begin
   process (aluResult)
   begin
      if aluResult=0 then aluZero <= '1';
      else aluZero <= '0';
      end if;
   end process;
end DataFlow;

