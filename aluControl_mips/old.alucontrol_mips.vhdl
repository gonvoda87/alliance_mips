library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

entity alucontrol_mips is
    port ( inst: in Std_Logic_Vector(5 downto 0);
          aluop: in Std_Logic_Vector (1 downto 0);
     alucontrol: out Std_Logic_Vector(3 downto 0));
end alucontrol_mips;

architecture DataFlow OF alucontrol_mips is
begin
   process (inst,aluop)
   begin
      if (aluop = "00") then alucontrol <= "0010";
      elsif (aluop = "01") then alucontrol <= "0110";
      elsif (aluop = "10") then 
         case inst is
           when "100000" => alucontrol <= "0010";   
           when "100010" => alucontrol <= "0110";           
           when "100100" => alucontrol <= "0000";           
           when "100101" => alucontrol <= "0001";           
           when "101010" => alucontrol <= "0111";          
           when others   => alucontrol <= alucontrol; 
         end case;
      elsif (aluop = "11") then alucontrol <= alucontrol;
      end if;
   end process; 
end DataFlow;

