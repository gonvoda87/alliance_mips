entity alucontrol_mips_o is
   port (
      inst       : in      bit_vector(5 downto 0);
      aluop      : in      bit_vector(1 downto 0);
      alucontrol : out     bit_vector(3 downto 0);
      vdd        : in      bit;
      vss        : in      bit
 );
end alucontrol_mips_o;

architecture structural of alucontrol_mips_o is
Component a2_x2
   port (
      i0  : in      bit;
      i1  : in      bit;
      q   : out     bit;
      vdd : in      bit;
      vss : in      bit
 );
end component;

Component na4_x1
   port (
      i0  : in      bit;
      i1  : in      bit;
      i2  : in      bit;
      i3  : in      bit;
      nq  : out     bit;
      vdd : in      bit;
      vss : in      bit
 );
end component;

Component inv_x2
   port (
      i   : in      bit;
      nq  : out     bit;
      vdd : in      bit;
      vss : in      bit
 );
end component;

Component a3_x2
   port (
      i0  : in      bit;
      i1  : in      bit;
      i2  : in      bit;
      q   : out     bit;
      vdd : in      bit;
      vss : in      bit
 );
end component;

Component nao22_x1
   port (
      i0  : in      bit;
      i1  : in      bit;
      i2  : in      bit;
      nq  : out     bit;
      vdd : in      bit;
      vss : in      bit
 );
end component;

Component no2_x1
   port (
      i0  : in      bit;
      i1  : in      bit;
      nq  : out     bit;
      vdd : in      bit;
      vss : in      bit
 );
end component;

Component na3_x1
   port (
      i0  : in      bit;
      i1  : in      bit;
      i2  : in      bit;
      nq  : out     bit;
      vdd : in      bit;
      vss : in      bit
 );
end component;

Component no4_x1
   port (
      i0  : in      bit;
      i1  : in      bit;
      i2  : in      bit;
      i3  : in      bit;
      nq  : out     bit;
      vdd : in      bit;
      vss : in      bit
 );
end component;

Component an12_x1
   port (
      i0  : in      bit;
      i1  : in      bit;
      q   : out     bit;
      vdd : in      bit;
      vss : in      bit
 );
end component;

signal not_aluop    : bit_vector( 0 downto 0);
signal not_inst     : bit_vector( 4 downto 0);
signal not_aux4     : bit;
signal not_aux2     : bit;
signal not_aux0     : bit;
signal no4_x1_sig   : bit;
signal no2_x1_sig   : bit;
signal na4_x1_sig   : bit;
signal na3_x1_sig   : bit;
signal inv_x2_sig   : bit;
signal inv_x2_3_sig : bit;
signal inv_x2_2_sig : bit;
signal aux3         : bit;
signal aux1         : bit;
signal a3_x2_sig    : bit;

begin

not_aux2_ins : an12_x1
   port map (
      i0  => inst(1),
      i1  => not_inst(4),
      q   => not_aux2,
      vdd => vdd,
      vss => vss
   );

not_aux4_ins : a2_x2
   port map (
      i0  => not_aluop(0),
      i1  => not_aux0,
      q   => not_aux4,
      vdd => vdd,
      vss => vss
   );

not_aux0_ins : a2_x2
   port map (
      i0  => inst(5),
      i1  => not_inst(3),
      q   => not_aux0,
      vdd => vdd,
      vss => vss
   );

not_inst_4_ins : inv_x2
   port map (
      i   => inst(4),
      nq  => not_inst(4),
      vdd => vdd,
      vss => vss
   );

not_inst_3_ins : inv_x2
   port map (
      i   => inst(3),
      nq  => not_inst(3),
      vdd => vdd,
      vss => vss
   );

not_inst_0_ins : inv_x2
   port map (
      i   => inst(0),
      nq  => not_inst(0),
      vdd => vdd,
      vss => vss
   );

not_aluop_0_ins : inv_x2
   port map (
      i   => aluop(0),
      nq  => not_aluop(0),
      vdd => vdd,
      vss => vss
   );

aux3_ins : no2_x1
   port map (
      i0  => inst(2),
      i1  => not_inst(0),
      nq  => aux3,
      vdd => vdd,
      vss => vss
   );

aux1_ins : a2_x2
   port map (
      i0  => inst(2),
      i1  => inst(1),
      q   => aux1,
      vdd => vdd,
      vss => vss
   );

inv_x2_ins : inv_x2
   port map (
      i   => aux1,
      nq  => inv_x2_sig,
      vdd => vdd,
      vss => vss
   );

na4_x1_ins : na4_x1
   port map (
      i0  => not_inst(4),
      i1  => not_inst(0),
      i2  => not_aux4,
      i3  => inv_x2_sig,
      nq  => na4_x1_sig,
      vdd => vdd,
      vss => vss
   );

alucontrol_0_ins : a2_x2
   port map (
      i0  => na4_x1_sig,
      i1  => aluop(1),
      q   => alucontrol(0),
      vdd => vdd,
      vss => vss
   );

alucontrol_1_ins : na4_x1
   port map (
      i0  => not_aux2,
      i1  => inst(2),
      i2  => aluop(1),
      i3  => not_aux4,
      nq  => alucontrol(1),
      vdd => vdd,
      vss => vss
   );

inv_x2_2_ins : inv_x2
   port map (
      i   => aluop(1),
      nq  => inv_x2_2_sig,
      vdd => vdd,
      vss => vss
   );

inv_x2_3_ins : inv_x2
   port map (
      i   => aux3,
      nq  => inv_x2_3_sig,
      vdd => vdd,
      vss => vss
   );

a3_x2_ins : a3_x2
   port map (
      i0  => not_aux2,
      i1  => not_aux0,
      i2  => inv_x2_3_sig,
      q   => a3_x2_sig,
      vdd => vdd,
      vss => vss
   );

alucontrol_2_ins : nao22_x1
   port map (
      i0  => a3_x2_sig,
      i1  => inv_x2_2_sig,
      i2  => not_aluop(0),
      nq  => alucontrol(2),
      vdd => vdd,
      vss => vss
   );

no2_x1_ins : no2_x1
   port map (
      i0  => inst(1),
      i1  => not_inst(3),
      nq  => no2_x1_sig,
      vdd => vdd,
      vss => vss
   );

na3_x1_ins : na3_x1
   port map (
      i0  => not_inst(4),
      i1  => inst(5),
      i2  => not_aluop(0),
      nq  => na3_x1_sig,
      vdd => vdd,
      vss => vss
   );

no4_x1_ins : no4_x1
   port map (
      i0  => aux3,
      i1  => aux1,
      i2  => na3_x1_sig,
      i3  => no2_x1_sig,
      nq  => no4_x1_sig,
      vdd => vdd,
      vss => vss
   );

alucontrol_3_ins : an12_x1
   port map (
      i0  => no4_x1_sig,
      i1  => aluop(1),
      q   => alucontrol(3),
      vdd => vdd,
      vss => vss
   );


end structural;
