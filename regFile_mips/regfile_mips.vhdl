library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

entity regfile_mips is
    port (  dataWrite: in Std_Logic_Vector(31 downto 0);
            addrReadReg1, addrReadReg2, addrWriteReg: in Std_Logic_Vector(4 downto 0);
            regWrite,clk: in Std_Logic ;
            data1,data2: out Std_Logic_Vector(31 downto 0));
end regfile_mips;

architecture DataFlow OF regfile_mips is
   type REGISTERS is array(31 downto 0) of Std_Logic_Vector(31 downto 0); 
   signal reg_array: REGISTERS;

begin
   data1 <= reg_array(addrReadReg1);
   data2 <= reg_array(addrReadReg2);

   process (clk,regWrite)
   begin
      if (clk'event and clk='1' and regWrite) then reg_array(addrWriteReg) <= dataWrite;
      end if;
   end process;

end DataFlow;
