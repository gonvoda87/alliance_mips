library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

entity shiftLeft_mips is
    port ( in0: in Std_Logic_Vector(31 downto 0);
           out0 : out Std_Logic_Vector(31 downto 0));
end shiftLeft_mips;

architecture DataFlow OF shiftLeft_mips is
begin
--   process (in0)
--   begin
--      out0 <= in0 << 2;
--   end process;
out0(31) <= in0(29);
out0(30) <= in0(28);
out0(29) <= in0(27);
out0(28) <= in0(26);
out0(27) <= in0(25);
out0(26) <= in0(24);
out0(25) <= in0(23);
out0(24) <= in0(22);
out0(23) <= in0(21);
out0(22) <= in0(20);
out0(21) <= in0(19);
out0(20) <= in0(18);
out0(19) <= in0(17);
out0(18) <= in0(16);
out0(17) <= in0(15);
out0(16) <= in0(14);
out0(15) <= in0(13);
out0(14) <= in0(12);
out0(13) <= in0(11);
out0(12) <= in0(10);
out0(11) <= in0(9);
out0(10) <= in0(8);
out0(9) <= in0(7);
out0(8) <= in0(6);
out0(7) <= in0(5);
out0(6) <= in0(4);
out0(5) <= in0(3);
out0(4) <= in0(2);
out0(3) <= in0(1);
out0(2) <= in0(0);
out0(1) <= '0';
out0(0) <= '0';

end DataFlow;

