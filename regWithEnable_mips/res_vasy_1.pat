
-- description generated by Pat driver

--			date     : Wed Jul  6 12:21:59 2016
--			revision : v109

--			sequence : regwithenable_mips

-- input / output list :
in       in0 (31 downto 0) X;;;
in       en B;;;
in       clk B;;;
out      out0 (31 downto 0) X;;;
in       vss B;;
in       vdd B;;

begin

-- Pattern description :

--                                                 i         e  c   o         v v  
--                                                 n         n  l   u         s d  
--                                                 0            k   t         s d  
--                                                                  0              

<          0 ps>                                 : 00000000  1  0  ?uuuuuuuu  0 1 ;
<          0 ps>                                 : 00000000  1  1  ?00000000  0 1 ;
<          0 ps>                                 : 00000001  1  0  ?00000000  0 1 ;
<          0 ps>                                 : 00000001  1  1  ?00000001  0 1 ;
<          0 ps>                                 : 00000010  1  0  ?00000001  0 1 ;
<          0 ps>                                 : 00000010  1  1  ?00000010  0 1 ;
<          0 ps>                                 : 00000011  1  0  ?00000010  0 1 ;
<          0 ps>                                 : 00000011  1  1  ?00000011  0 1 ;
<          0 ps>                                 : 00000100  1  0  ?00000011  0 1 ;
<          0 ps>                                 : 00000100  1  1  ?00000100  0 1 ;
<          0 ps>                                 : 00000101  1  0  ?00000100  0 1 ;
<          0 ps>                                 : 00000101  1  1  ?00000101  0 1 ;
<          0 ps>                                 : 00000110  1  0  ?00000101  0 1 ;
<          0 ps>                                 : 00000110  1  1  ?00000110  0 1 ;
<          0 ps>                                 : 00000111  1  0  ?00000110  0 1 ;
<          0 ps>                                 : 00000111  1  1  ?00000111  0 1 ;
<          0 ps>                                 : 00001000  1  0  ?00000111  0 1 ;
<          0 ps>                                 : 00001000  1  1  ?00001000  0 1 ;
<          0 ps>                                 : 00001001  1  0  ?00001000  0 1 ;
<          0 ps>                                 : 00001001  1  1  ?00001001  0 1 ;
<          0 ps>                                 : 00001010  1  0  ?00001001  0 1 ;
<          0 ps>                                 : 00001010  1  1  ?00001010  0 1 ;
<          0 ps>                                 : 00000000  0  0  ?00001010  0 1 ;
<          0 ps>                                 : 00000000  0  1  ?00000000  0 1 ;
<          0 ps>                                 : 00000001  0  0  ?00000000  0 1 ;
<          0 ps>                                 : 00000001  0  1  ?00000000  0 1 ;
<          0 ps>                                 : 00000010  0  0  ?00000000  0 1 ;
<          0 ps>                                 : 00000010  0  1  ?00000000  0 1 ;
<          0 ps>                                 : 00000011  0  0  ?00000000  0 1 ;
<          0 ps>                                 : 00000011  0  1  ?00000000  0 1 ;
<          0 ps>                                 : 00000100  0  0  ?00000000  0 1 ;
<          0 ps>                                 : 00000100  0  1  ?00000000  0 1 ;
<          0 ps>                                 : 00000101  0  0  ?00000000  0 1 ;
<          0 ps>                                 : 00000101  0  1  ?00000000  0 1 ;
<          0 ps>                                 : 00000110  0  0  ?00000000  0 1 ;
<          0 ps>                                 : 00000110  0  1  ?00000000  0 1 ;
<          0 ps>                                 : 00000111  0  0  ?00000000  0 1 ;
<          0 ps>                                 : 00000111  0  1  ?00000000  0 1 ;
<          0 ps>                                 : 00001000  0  0  ?00000000  0 1 ;
<          0 ps>                                 : 00001000  0  1  ?00000000  0 1 ;
<          0 ps>                                 : 00001001  0  0  ?00000000  0 1 ;
<          0 ps>                                 : 00001001  0  1  ?00000000  0 1 ;
<          0 ps>                                 : 00001010  0  0  ?00000000  0 1 ;
<          0 ps>                                 : 00001010  0  1  ?00000000  0 1 ;

end;
