library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

entity register_mips is
    port (  in0: in Std_Logic_Vector(31 downto 0);
             clk,en: in Std_Logic ;
           out0: out Std_Logic_Vector(31 downto 0));
end register_mips;

architecture DataFlow OF register_mips is
begin
   process (en,clk)
   variable enable: Std_Logic_Vector(31 downto 0);
   begin
      FOR i IN in0'RANGE LOOP
         enable(i) := in0(i) AND en;
      END LOOP;
--      out0 <= enable;
--        if(en = '0') then out0 <= '0';
    if (clk'event and clk='1') then out0<=enable;
    end if;
   end process;
end DataFlow;
