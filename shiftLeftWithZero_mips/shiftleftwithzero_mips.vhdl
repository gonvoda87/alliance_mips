library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

entity shiftLeft_mips is
    port ( in0: in Std_Logic_Vector(25 downto 0);
           out0 : out Std_Logic_Vector(27 downto 0));
end shiftLeft_mips;

architecture DataFlow OF shiftLeft_mips is
--signal shift: std_logic_vector(29 downto 0);
begin
--      out0 <= in0 SLL 2;
   out0(25 downto 0) <= in0;  
   out0(27 downto 26) <= 0; 
end DataFlow;

