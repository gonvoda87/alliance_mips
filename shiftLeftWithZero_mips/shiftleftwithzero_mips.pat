in       in0 (25 downto 0) B;;;
out      out0 (27 downto 0) B;;;
in       vss B;;
in       vdd B;;

begin

-- Pattern description :
--                  in0                           out                    V V
<   0ns>: 11111111111111111111111111  ?****************************      0 1;
<  10ns>: 11111111111111111111111111  ?****************************      0 1;
end;
