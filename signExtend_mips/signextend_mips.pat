in       in0 (15 downto 0) X;;;
out      out0 (31 downto 0) X;;;
in       vss B;;
in       vdd B;;

begin

-- Pattern description :
--         in0     out         V V
<   0ns>: FFFF  ?********      0 1;
<  10ns>: FFFF  ?********      0 1;
<  10ns>: 8000  ?********      0 1;
<  10ns>: 8000  ?********      0 1;
<  10ns>: 0000  ?********      0 1;
<  10ns>: 0000  ?********      0 1;
<  10ns>: 7FFF  ?********      0 1;
<  10ns>: 7FFF  ?********      0 1;
end;
