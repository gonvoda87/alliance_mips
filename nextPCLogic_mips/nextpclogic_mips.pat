in       write B;;;
in       writecond B;;;
in       zero B;;;
out      out0 B;;;
in       vss B;;
in       vdd B;;

begin

-- Pattern description :
--        write  writecond   zero   out         V V
<   0ns>:   0       0         0      ?*         0 1;
<  10ns>:   0       0         0      ?*         0 1;
<  10ns>:   0       0         1      ?*         0 1;
<  10ns>:   0       0         1      ?*         0 1;
<  10ns>:   0       1         0      ?*         0 1;
<  10ns>:   0       1         0      ?*         0 1;
<  10ns>:   0       1         1      ?*         0 1;
<  10ns>:   0       1         1      ?*         0 1;
<  10ns>:   1       0         0      ?*         0 1;
<  10ns>:   1       0         0      ?*         0 1;
<  10ns>:   1       0         1      ?*         0 1;
<  10ns>:   1       0         1      ?*         0 1;
<  10ns>:   1       1         0      ?*         0 1;
<  10ns>:   1       1         0      ?*         0 1;
<  10ns>:   1       1         1      ?*         0 1;
<  10ns>:   1       1         1      ?*         0 1;


end;
