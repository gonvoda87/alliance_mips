library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

entity mux4to1_mips is
    port ( in0,in1,in2,in3: in Std_Logic_Vector(31 downto 0);
               sel: in Std_Logic_Vector(1 downto 0);
           out4x1 : out Std_Logic_Vector(31 downto 0));
end mux4to1_mips;

architecture DataFlow OF mux4to1_mips is
begin
   process(in0,in1,in2,in3,sel)
   begin
--    out2x1 <= in0 WHEN (sel = '1') ELSE in1;
      CASE sel IS 
         WHEN "00" => out4x1 <= in0;  
         WHEN "01" => out4x1 <= in1;
         WHEN "10" => out4x1 <= in2;
         WHEN "11" => out4x1 <= in3;
      END CASE ;
   end process;
end DataFlow;
