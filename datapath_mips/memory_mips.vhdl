library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

entity memory_mips is
    port ( memoryWriteData, memoryAddress: in Std_Logic_Vector(31 downto 0);
           clk, memoryWrite, memoryRead: in Std_Logic ;
           memoryOutData: out Std_Logic_Vector(31 downto 0));
end memory_mips;

architecture DataFlow OF memory_mips is
   type REGISTERS is array(31 downto 0) of Std_Logic_Vector(31 downto 0);
   signal storage: REGISTERS;
   signal Address: Std_Logic_Vector(31 downto 0);
   signal enable_signal: Std_Logic;
begin
Address(29 downto 0) <= memoryAddress(31 downto 2);
Address(31 downto 30) <= 0;
--Lectura
   process (clk,Address,memoryWrite,memoryRead)
   begin
--      enable_signal <= not memoryWrite and memoryRead;
      if(not memoryWrite and memoryRead) then
        if (clk'event and clk='1') then memoryOutData <= Storage(Address);
        end if;
      else memoryOutData <= memoryOutData;
      end if;
   end process;  
--Escritura
   process (clk,memoryWrite,memoryRead,Address,memoryWriteData)
   begin
      if (memoryWrite and not memoryRead) then 
         if (clk'event and clk='1') then Storage(Address) <= memoryWriteData;
         end if;
      end if;
   end process;
end DataFlow;
