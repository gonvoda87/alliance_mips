library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

entity signextend_mips is
    port ( in0: in Std_Logic_Vector(15 downto 0);
           out0 : out Std_Logic_Vector(31 downto 0));
end signextend_mips;

architecture DataFlow OF signextend_mips is
begin
   process (in0)
   begin
      out0(15 downto 0) <= in0;  
      case in0(15) is
        when '0' => out0(31 downto 16) <= '0'; 
        when '1' => out0(31 downto 16) <= "1111111111111111";
      end case;
   end process; 
end DataFlow;

