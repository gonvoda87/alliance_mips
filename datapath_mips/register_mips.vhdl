library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

entity register_mips is
    port (  in0: in Std_Logic_Vector(31 downto 0);
            clk: in Std_Logic ;
           out0: out Std_Logic_Vector(31 downto 0));
end register_mips;

architecture DataFlow OF register_mips is
begin
    process (clk)
    begin
    if (clk'event and clk='1') then out0<=in0;
    end if;
    end process;
end DataFlow;
