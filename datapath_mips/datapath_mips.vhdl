library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

entity datapath_mips is
    port ( PCSource,ALUSrcB: in Std_Logic_Vector(1 downto 0);
           ALUControl: in Std_Logic_Vector(3 downto 0);
           ALUSrcA, RegWrite, RegDest, PCWriteCond, PCWrite, IorD, MemRead, MemWrite, MemtoReg, IRWrite, clk: in Std_Logic;
           ALUresult, Instruction: out  Std_Logic_Vector(31 downto 0);
           overflow: out Std_Logic);
end datapath_mips;

architecture DataFlow OF datapath_mips is
   signal mux4_0_out: Std_Logic_Vector(31 downto 0);
   signal ALUOut_out: Std_Logic_Vector(31 downto 0);
   signal mux2_0_out: Std_Logic_Vector(31 downto 0);
   signal mux2_1_out: Std_Logic_Vector(31 downto 0);
   signal mux2_2_out: Std_Logic_Vector(31 downto 0);
   signal mux2_3_out: Std_Logic_Vector(31 downto 0);
   signal regB_out: Std_Logic_Vector(31 downto 0);
   signal regA_out: Std_Logic_Vector(31 downto 0);
   signal memory_out: Std_Logic_Vector(31 downto 0);
   signal reg_mem_data_out: Std_Logic_Vector(31 downto 0);
   signal instruction_register_out: Std_Logic_Vector(31 downto 0);
   signal register_out_1: Std_Logic_Vector(31 downto 0);
   signal register_out_2: Std_Logic_Vector(31 downto 0);
   signal sign_extend_out: Std_Logic_Vector(31 downto 0);
   signal shift_left_out: Std_Logic_Vector(31 downto 0);
   signal shift_left_zero_out: Std_Logic_Vector(27 downto 0);
   signal mux4_1_out: Std_Logic_Vector(31 downto 0);
   signal NextPCLogic_out: Std_Logic;
   signal pc_out: Std_Logic_Vector(31 downto 0);
   signal ALU_output: Std_Logic_Vector(31 downto 0);
   signal ALUZero_out: Std_Logic;

component memory_mips port (
   memoryWriteData: in Std_Logic_Vector(31 downto 0);
   memoryAddress: in Std_Logic_Vector(31 downto 0);
   memoryWrite: in Std_Logic;
   memoryRead: in Std_Logic;
   clk: in Std_Logic;
   memoryOutData: out Std_Logic_Vector(31 downto 0));
end component;

component regWithEnable_mips port (  
   in0: in Std_Logic_Vector(31 downto 0);
   clk,en: in Std_Logic ;
   out0: out Std_Logic_Vector(31 downto 0));
end component;

component register_mips port (
   in0: in Std_Logic_Vector(31 downto 0);
   clk: in Std_Logic ;
   out0: out Std_Logic_Vector(31 downto 0));
end component;

component alu_mips port ( 
   aluInA, aluInB: in Std_Logic_Vector(31 downto 0);
   aluControl: in Std_Logic_Vector (3 downto 0);
   aluResult: out Std_Logic_Vector(31 downto 0);
   aluZero,aluOverflow: out Std_Logic);
end component;

component shiftLeftWithZero_mips port ( 
   in0: in Std_Logic_Vector(25 downto 0);
   out0: out Std_Logic_Vector(27 downto 0));
end component;

component mux2to1_mips port (
   in0,in1: in Std_Logic_Vector(31 downto 0);
   sel: in Std_Logic ;
   out2x1 : out Std_Logic_Vector(31 downto 0));
end component;

component mux4to1_mips port (
   in0,in1,in2,in3: in Std_Logic_Vector(31 downto 0);
   sel: in Std_Logic_Vector(1 downto 0);
   out4x1 : out Std_Logic_Vector(31 downto 0));
end component;

component nextpclogic_mips port ( 
   zero,writecond, write: in Std_Logic;
   out0: out Std_Logic);
end component;

component regfile_mips port (
   dataWrite: in Std_Logic_Vector(31 downto 0);
   addrReadReg1, addrReadReg2, addrWriteReg: in Std_Logic_Vector(4 downto 0);
   regWrite,clk: in Std_Logic ;
   data1,data2: out Std_Logic_Vector(31 downto 0));
end component;

component signextend_mips port (
   in0: in Std_Logic_Vector(15 downto 0);
   out0 : out Std_Logic_Vector(31 downto 0));
end component;

begin

ALUresult <= ALUOut_out;
Instruction <= sign_extend_out;

MEMORY: memory_mips  port map (
 memoryWriteData => regB_out,
 memoryAddress   => mux2_0_out,
 memoryWrite     => MemWrite,
 memoryRead      => MemRead,
 clk             => clk,
 memoryOutData   => memory_out);

INSTRUCTION_REGISTER: regWithEnable_mips  port map ( 
 in0  => memory_out, 
 clk  => clk, 
 en   => IRWrite, 
 out0 => instruction_register_out);

PC: regWithEnable_mips port map (
 in0  => mux4_1_out,
 clk  => clk,
 en   => NextPCLogic_out,
 out0 => pc_out);

REG_ALU_OUT: register_mips port map (
 in0  => ALU_output,
 clk  => clk, 
 out0 => ALUOut_out);

REG_A: register_mips port map (
 in0  => register_out_1,
 clk  => clk, 
 out0 => regA_out);

REG_B: register_mips port map (
 in0  => register_out_2,
 clk  => clk,  
 out0 => regB_out);

REG_MEM_DATA: register_mips port map (
 in0  => memory_out, 
 clk  => clk, 
 out0 => reg_mem_data_out);

ALU: alu_mips port map (
 aluInA      => mux2_3_out, 
 aluInB      => mux4_0_out, 
 aluControl  => ALUControl, 
 aluResult   => ALU_output,
 aluZero     => ALUZero_out, 
 aluOverflow => overflow);

SHIFT_LEFT_Z: shiftLeftWithZero_mips port map ( 
 in0  => instruction_register_out(25 downto 0),
 out0 => shift_left_zero_out);

MUX2_0: mux2to1_mips port map (
 in0    => pc_out,
 in1    => ALUOut_out,
 sel    => IorD,
 out2x1 => mux2_0_out);

MUX2_1: mux2to1_mips port map (
 in0    => instruction_register_out(20 downto 16),
 in1    => instruction_register_out(15 downto 11),
 sel    => RegDest,
 out2x1 => mux2_1_out);

MUX2_2: mux2to1_mips port map (
 in0    => ALUOut_out,
 in1    => reg_mem_data_out,
 sel    => MemtoReg,
 out2x1 => mux2_2_out);

MUX3_2: mux2to1_mips port map (
 in0    => pc_out,
 in1    => regA_out,
 sel    => ALUSrcA,
 out2x1 => mux2_3_out);

MUX4_0: mux4to1_mips port map (
 in0    => regB_out,
 in1    => x"00000004",
 in2    => sign_extend_out,
 in3    => shift_left_out,
 sel    => ALUSrcB,
 out4x1 => mux4_0_out);

MUX4_1: mux4to1_mips port map (
 in0    => ALU_output,
 in1    => ALUOut_out,
 in2    => 0000 & shift_left_zero_out,
 in3    => shift_left_out,
 sel    => PCSource,
 out4x1 => mux4_1_out);

PC_LOGIC: nextPCLogic_mips port map (
 zero      => ALUZero_out,
 writecond => PCWriteCond,
 write     => PCWrite,
 out0      => NextPCLogic_out);

REGISTERS: regfile_mips port map ( 
 dataWrite    => mux2_2_out, 
 addrReadReg1 => instruction_register_out(25 downto 21),
 addrReadReg2 => instruction_register_out(20 downto 16), 
 addrWriteReg => mux2_1_out,
 regWrite     => RegWrite,
 clk          => clk,
 data1        => register_out_1,
 data2        => register_out_2);

SIGN_EXTEND: signExtend_mips port map (
 in0  => instruction_register_out(15 downto 0),
 out0 => sign_extend_out);

end DataFlow;
