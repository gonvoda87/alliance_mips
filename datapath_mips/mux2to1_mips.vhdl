library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

entity mux2to1_mips is
    port ( in0,in1: in Std_Logic_Vector(31 downto 0);
               sel: in Std_Logic ;
           out2x1 : out Std_Logic_Vector(31 downto 0));
end mux2to1_mips;

architecture DataFlow OF mux2to1_mips is
begin
    out2x1 <= in0 WHEN (sel = '1') ELSE in1;
--out2x1 <= in0 + in1 + sel;
end DataFlow;
