library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

entity alucontrol_mips is
    port ( inst: in Std_Logic_Vector(5 downto 0);
          aluop: in Std_Logic_Vector (1 downto 0);
     alucontrol: out Std_Logic_Vector(3 downto 0));
end alucontrol_mips;

architecture DataFlow OF alucontrol_mips is
signal control_temp: Std_Logic_Vector(3 downto 0);
begin
   process (inst)
   begin
      case inst is
         when "100000" => control_temp <= "0010";
         when "100010" => control_temp <= "0110";
         when "100100" => control_temp <= "0000";
         when "100101" => control_temp <= "0001";
         when "101010" => control_temp <= "0111";
         when others   => control_temp <= "1111";
     end case;
   end process;

   process (aluop)
   begin
      case aluop is
         when "00" => alucontrol <= "0010";
         when "01" => alucontrol <= "0110";
         when "10" => alucontrol <= control_temp;
         when "11" => alucontrol <= "1111";
     end case;
   end process; 
end DataFlow;

