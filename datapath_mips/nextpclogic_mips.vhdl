library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

entity nextpclogic_mips is
    port (zero,writecond, write: in Std_Logic;
           out0: out Std_Logic);
end nextpclogic_mips;

architecture DataFlow OF nextpclogic_mips is
begin
out0 <= (zero and writecond) or write; 
end DataFlow;

