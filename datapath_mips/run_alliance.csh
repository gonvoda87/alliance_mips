#!/bin/csh -f
######################## Config ################################
setenv DESIGN datapath_mips
setenv TARGET_LIB "../etc/cells/sxlib"
setenv METAL_LEVEL 2
#################################################################
setenv MBK_WORK_LIB ./
setenv MBK_CATAL_NAME NO_CATAL

/usr/bin/vasy  -a -B -o -p -I vhdl -H ${DESIGN}

setenv MBK_CATAL_NAME CATAL_ASIMUT_VASY
setenv MBK_IN_LO vst
setenv MBK_OUT_LO vst

#/usr/bin/asimut ${DESIGN} ${DESIGN} res_vasy_1


/usr/bin/boom -VP ${DESIGN}_model ${DESIGN}_o

setenv MBK_TARGET_LIB $TARGET_LIB

/usr/bin/boog ${DESIGN}_o

setenv MBK_CATA_LIB $TARGET_LIB

/usr/bin/loon  ${DESIGN}_o ${DESIGN}
